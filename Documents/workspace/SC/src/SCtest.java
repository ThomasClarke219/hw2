import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class SCtest extends Application {
	@Override
	public void start(Stage myStage) {

		myStage.setTitle("Calculator");

		GridPane rootNode = new GridPane();
		rootNode.setPadding(new Insets(15));
		rootNode.setHgap(5);
		rootNode.setVgap(5);
		rootNode.setAlignment(Pos.CENTER);

		Scene myScene = new Scene(rootNode, 300, 300);

		rootNode.add(new Label("Goal Value:"), 0, 0);
		TextField goal1 = new TextField();
		rootNode.add(goal1, 1, 0);

		rootNode.add(new Label("Starting Coins:"), 0, 1);
		TextField start1 = new TextField();
		rootNode.add(start1, 1, 1);

		rootNode.add(new Label("Daily Steps:"), 0, 2);
		TextField daily1 = new TextField();
		rootNode.add(daily1, 1, 2);

		rootNode.add(new Label("Fee:"), 0, 3);
		TextField ded = new TextField();
		rootNode.add(ded, 1, 3);
		Button aButton = new Button("Calculate");
		rootNode.add(aButton, 1, 4);

		GridPane.setHalignment(aButton, HPos.LEFT);
		TextField result = new TextField();
		result.setEditable(false);
		Label q = new Label("");
		rootNode.add(q, 1, 5);
		aButton.setOnAction(e -> {
			Integer goal = Integer.valueOf(goal1.getText());
			Integer start = Integer.valueOf(start1.getText());
			Integer daily = Integer.valueOf(daily1.getText());
			int total = start;
			int days = 1;
			int steps = 0;
			while (total < goal) {
				if (days % 30 == 0) {
					total -= Integer.valueOf(ded.getText());;
				}
				if ((goal - total) > ((daily / 1000) * .95)) {
					total += (daily / 1000) * .95;
					steps += daily;
					days++;
				} else {
					steps += (int) ((goal - total) / .00095);
					total = goal;
					days++;
				}
			}
			q.setText("It would take you:\n" + numRight(steps) + " steps\nThrough " + numRight(days) + " days");
		});

		myStage.setScene(myScene);

		myStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}

	public static String numRight(int num) {
		if (num > 1000) {
			int i = 0;
			String ret = "";
			String numb = Integer.toString(num);
			for (int m = numb.length(); m > 0; m--) {
				ret = numb.substring(m - 1, m) + ret;
				i++;
				if (i == 3) {
					i = 0;
					ret = "," + ret;
				}
			}
			return ret;
		} else
			return Integer.toString(num);
	}
}