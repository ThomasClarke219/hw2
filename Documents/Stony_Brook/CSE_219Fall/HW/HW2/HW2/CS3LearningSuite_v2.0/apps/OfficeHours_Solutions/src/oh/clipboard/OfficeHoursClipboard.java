package oh.clipboard;

import static djf.AppPropertyType.PASTE_BUTTON;
import djf.components.AppClipboardComponent;
import djf.modules.AppGUIModule;
import java.util.ArrayList;
import java.util.Collections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import oh.OfficeHoursApp;
import static oh.OfficeHoursPropertyType.OH_OFFICE_HOURS_TABLE_VIEW;
import static oh.OfficeHoursPropertyType.OH_TAS_TABLE_VIEW;
import oh.data.OfficeHoursData;
import oh.data.TeachingAssistantPrototype;
import oh.data.TimeSlot;
import oh.transactions.AddTA_Transaction;
import oh.transactions.RemoveTA_Transaction;

/**
 *
 * @author McKillaGorilla
 */
public class OfficeHoursClipboard implements AppClipboardComponent {

    OfficeHoursApp app;
    OfficeHoursData data;
    
    // just store the cut/copied ta here
    ArrayList<TeachingAssistantPrototype> clipboardCutItems;
    ArrayList<TeachingAssistantPrototype> clipboardCopiedItems;

    public OfficeHoursClipboard(OfficeHoursApp initApp) {
        app = initApp;
        clipboardCutItems = new ArrayList<>();
        clipboardCopiedItems = new ArrayList<>();
    }

    @Override
    public void cut() {
        if (hasSomethingToCut()) {
            data = (OfficeHoursData) app.getDataComponent();
            AppGUIModule gui = app.getGUIModule();

            TableView<TeachingAssistantPrototype> taTableView = (TableView) gui.getGUINode(OH_TAS_TABLE_VIEW);
            TeachingAssistantPrototype ta = taTableView.getSelectionModel().getSelectedItem();
            
            TableView officeHoursTableView = (TableView) gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);
            ObservableList<TimeSlot> officeHours = officeHoursTableView.getItems();
            for(TimeSlot slot: officeHours)
            {
                slot.cutTA(ta);
            }
            
            clipboardCopiedItems.add(ta);
            clipboardCutItems.add(ta);
            RemoveTA_Transaction removeTATransaction = new RemoveTA_Transaction(data, ta);
            app.processTransaction(removeTATransaction);

            // Now remove the selected TA
            Button paste = (Button) gui.getGUINode(PASTE_BUTTON);
            Collections.sort(taTableView.getItems());
        }
    }

    @Override
    public void copy() {
        if (hasSomethingToCopy()) {
            AppGUIModule gui = app.getGUIModule();

            // Get the selected Item
            TableView<TeachingAssistantPrototype> taTableView = (TableView) gui.getGUINode(OH_TAS_TABLE_VIEW);
            TeachingAssistantPrototype ta = taTableView.getSelectionModel().getSelectedItem();

            clipboardCopiedItems.add(ta);

            Button paste = (Button) gui.getGUINode(PASTE_BUTTON);
            paste.setDisable(false);
        }
    }

    @Override
    public void paste() {
        if (hasSomethingToPaste()) {
            Collections.sort(clipboardCopiedItems);
            data = (OfficeHoursData) app.getDataComponent();
            AppGUIModule gui = app.getGUIModule();
            TableView<TeachingAssistantPrototype> taTableView = (TableView) gui.getGUINode(OH_TAS_TABLE_VIEW);
            for (TeachingAssistantPrototype ta : clipboardCopiedItems) {
                TeachingAssistantPrototype holdingTA;
                if (taTableView.getItems().contains(ta)) {
                    String numID = ((int) (Math.random() * 1000)) + "";
                    String newName = ta.getName() + numID;
                    String newEmail = ta.getEmail().split("@")[0] + numID + "@"+ ta.getEmail().split("@")[1];
                    holdingTA = new TeachingAssistantPrototype(newName, newEmail, ta.getType());
                } else {
                    holdingTA = ta;
                }
                holdingTA.setSlots("0");
                AddTA_Transaction addTATransaction = new AddTA_Transaction(data, holdingTA);
                app.processTransaction(addTATransaction);
            }
            Collections.sort(taTableView.getItems());
        }
    }

    @Override
    public boolean hasSomethingToCut() {
        return ((OfficeHoursData) app.getDataComponent()).isTASelected();
    }

    @Override
    public boolean hasSomethingToCopy() {
        return ((OfficeHoursData) app.getDataComponent()).isTASelected();
    }

    @Override
    public boolean hasSomethingToPaste() {
        if ((clipboardCutItems != null) && (!clipboardCutItems.isEmpty())) {
            return true;
        } else if ((clipboardCopiedItems != null) && (!clipboardCopiedItems.isEmpty())) {
            return true;
        } else {
            return false;
        }
    }
}
