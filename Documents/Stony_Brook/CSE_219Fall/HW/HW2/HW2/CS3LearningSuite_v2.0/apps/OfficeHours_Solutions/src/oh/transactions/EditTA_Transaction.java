package oh.transactions;

import static djf.AppPropertyType.SAVE_BUTTON;
import djf.modules.AppGUIModule;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import jtps.jTPS_Transaction;
import static oh.OfficeHoursPropertyType.OH_OFFICE_HOURS_TABLE_VIEW;
import static oh.OfficeHoursPropertyType.OH_TAS_TABLE_VIEW;
import oh.data.TeachingAssistantPrototype;
import oh.data.TimeSlot;

/**
 *
 * @author thomas
 */
public class EditTA_Transaction implements jTPS_Transaction {

    final TeachingAssistantPrototype changeTA;
    TeachingAssistantPrototype newerTA;
    TeachingAssistantPrototype statusTA;
    AppGUIModule gui;

    public EditTA_Transaction(TeachingAssistantPrototype oldTA, TeachingAssistantPrototype newTA, AppGUIModule theGUI) {
        changeTA = oldTA;
        newerTA = newTA;
        
        gui = theGUI;

        statusTA = new TeachingAssistantPrototype(oldTA.getName(), oldTA.getEmail(), oldTA.getType());
    }

    @Override
    public void doTransaction() {
        changeTA.changeValues(newerTA);
        TableView tasTableView = (TableView) gui.getGUINode(OH_TAS_TABLE_VIEW);
        TableView officeHoursTableView = (TableView) gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);
        tasTableView.refresh();
        
        for (int i = 0; i < officeHoursTableView.getItems().size(); i++) {
                TimeSlot timeslot = (TimeSlot) officeHoursTableView.getItems().get(i);
                timeslot.refresh();
            }
        Button a = (Button) gui.getGUINode(SAVE_BUTTON);
        a.setDisable(false);
    }

    @Override
    public void undoTransaction() {
        changeTA.changeValues(statusTA);
        TableView tasTableView = (TableView) gui.getGUINode(OH_TAS_TABLE_VIEW);
        TableView officeHoursTableView = (TableView) gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);
        tasTableView.refresh();
        
        for (int i = 0; i < officeHoursTableView.getItems().size(); i++) {
                TimeSlot timeslot = (TimeSlot) officeHoursTableView.getItems().get(i);
                timeslot.refresh();
            }
        Button a = (Button) gui.getGUINode(SAVE_BUTTON);
        a.setDisable(false);
    }
}
