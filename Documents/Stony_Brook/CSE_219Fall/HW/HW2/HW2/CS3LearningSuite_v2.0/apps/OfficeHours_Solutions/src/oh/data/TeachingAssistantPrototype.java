package oh.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This class represents a Teaching Assistant for the table of TAs.
 *
 * @author Richard McKenna
 */
public class TeachingAssistantPrototype<E extends Comparable<E>> implements Comparable<E> {

    // THE TABLE WILL STORE TA NAMES AND EMAILS
    private final StringProperty name;
    private final StringProperty email;
    private final StringProperty slots;
    private final StringProperty type;

    /**
     * Constructor initializes both the TA name and email.
     */
    public TeachingAssistantPrototype(String initName, String initEmail) {
        name = new SimpleStringProperty(initName);
        email = new SimpleStringProperty(initEmail);
        type = new SimpleStringProperty("Graduate");
        slots = new SimpleStringProperty("" + 0);
    }

    public TeachingAssistantPrototype(String initName, String initEmail, String initType) {
        name = new SimpleStringProperty(initName);
        email = new SimpleStringProperty(initEmail);
        type = new SimpleStringProperty(initType);
        slots = new SimpleStringProperty("" + 0);
    }

    // ACCESSORS AND MUTATORS FOR THE PROPERTIES
    public String getName() {
        return name.get();
    }

    public void setName(String initName) {
        name.set(initName);
    }

    public String getEmail() {
        return email.get();
    }

    public void setEmail(String initEmail) {
        email.set(initEmail);
    }

    public String getType() {
        return type.get();
    }

    public void setType(String initType) {
        type.set(initType);
    }

    public String getSlots() {
        return slots.get();
    }

    public void setSlots(String initSlots) {
        slots.setValue(initSlots);
    }

    public StringProperty slotsProperty() {
        return slots;
    }

    @Override
    public int compareTo(E otherTA) {
        return getName().compareTo(((TeachingAssistantPrototype) otherTA).getName());
    }

    @Override
    public String toString() {
        return name.getValue() + " " + email.getValue() + " " + type.getValue();
    }

    public boolean equals(TeachingAssistantPrototype testTA) {
        return     name.getValue().toLowerCase().equals((testTA).name.getValue().toLowerCase().trim())
                && email.getValue().toLowerCase().equals((testTA).email.getValue().toLowerCase().trim()) 
                && type.getValue().toLowerCase().equals((testTA).type.getValue().toLowerCase().trim());
    }

    public void changeValues(TeachingAssistantPrototype newTA) {
        setEmail(newTA.getEmail());
        setName(newTA.getName());
        setType(newTA.getType());
    }

    void changeTimeSlotCount(int i) {
        int count = Integer.parseInt(this.slots.get());
        count += i;
        this.setSlots("" + count);
    }
}
