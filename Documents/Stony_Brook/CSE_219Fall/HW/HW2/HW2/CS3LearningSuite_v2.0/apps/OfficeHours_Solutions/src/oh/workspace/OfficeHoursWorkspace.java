package oh.workspace;

import static djf.AppPropertyType.COPY_BUTTON;
import static djf.AppPropertyType.CUT_BUTTON;
import djf.components.AppWorkspaceComponent;
import djf.modules.AppFoolproofModule;
import djf.modules.AppGUIModule;
import static djf.modules.AppGUIModule.ENABLED;
import djf.ui.AppNodesBuilder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import oh.OfficeHoursApp;
import oh.OfficeHoursPropertyType;
import static oh.OfficeHoursPropertyType.*;
import oh.clipboard.OfficeHoursClipboard;
import oh.data.OfficeHoursData;
import oh.data.TeachingAssistantPrototype;
import oh.data.TimeSlot;
import oh.data.TimeSlot.DayOfWeek;
import oh.transactions.EditTA_Transaction;
import oh.workspace.controllers.OfficeHoursController;
import oh.workspace.foolproof.OfficeHoursFoolproofDesign;
import static oh.workspace.style.OHStyle.*;

/**
 *
 * @author McKillaGorilla
 */
public class OfficeHoursWorkspace extends AppWorkspaceComponent {

    OfficeHoursClipboard clipper;

    // For the Radio Button use
    ArrayList<TeachingAssistantPrototype> holdTA;
    ArrayList<DayOfWeek> dayOfWeek;
    TableView holdOfficeHoursTable;
    
    OfficeHoursData data;
    boolean dataSet;
    
    // Use for Editing TA
    String newName;
    String newEmail;
    String newType;

    public OfficeHoursWorkspace(OfficeHoursApp app) {
        super(app);
        dataSet = false;
        
        holdTA = new ArrayList<>();
        dayOfWeek = new ArrayList<>();

        dayOfWeek.add(DayOfWeek.MONDAY);
        dayOfWeek.add(DayOfWeek.TUESDAY);
        dayOfWeek.add(DayOfWeek.WEDNESDAY);
        dayOfWeek.add(DayOfWeek.THURSDAY);
        dayOfWeek.add(DayOfWeek.FRIDAY);

        // LAYOUT THE APP
        initLayout();

        // INIT THE EVENT HANDLERS
        initControllers();

        // INIT THE SAFTEY PROOFING
        initFoolproofDesign();
    }

    // THIS HELPER METHOD INITIALIZES ALL THE CONTROLS IN THE WORKSPACE
    private void initLayout() {
        // FIRST LOAD THE FONT FAMILIES FOR THE COMBO BOX
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // THIS WILL BUILD ALL OF OUR JavaFX COMPONENTS FOR US
        AppNodesBuilder ohBuilder = app.getGUIModule().getNodesBuilder();
         
        clipper = new OfficeHoursClipboard((OfficeHoursApp) app);

        // INIT THE HEADER ON THE LEFT
        VBox leftPane = ohBuilder.buildVBox(OH_LEFT_PANE, null, CLASS_OH_PANE, ENABLED);
        HBox tasHeaderBox = ohBuilder.buildHBox(OH_TAS_HEADER_PANE, leftPane, CLASS_OH_BOX, ENABLED);
        ohBuilder.buildLabel(OfficeHoursPropertyType.OH_TAS_HEADER_LABEL, tasHeaderBox, CLASS_OH_HEADER_LABEL, ENABLED);

        // ADD THE RADIO BUTTONS FOR THE TA TYPE
        ToggleGroup typeButton = new ToggleGroup();
        RadioButton all = ohBuilder.buildRadioButton(OH_RADIO_ALL, tasHeaderBox, CLASS_OH_RADIO, ENABLED);
        RadioButton graduate = ohBuilder.buildRadioButton(OH_RADIO_GRADUATE, tasHeaderBox, CLASS_OH_RADIO, ENABLED);
        RadioButton underGraduate = ohBuilder.buildRadioButton(OH_RADIO_UNDERGRADUATE, tasHeaderBox, CLASS_OH_RADIO, ENABLED);

        // Add them to the same activation group
        all.setToggleGroup(typeButton);
        graduate.setToggleGroup(typeButton);
        underGraduate.setToggleGroup(typeButton);
        all.setSelected(true);

        // MAKE THE TABLE AND SETUP THE DATA MODEL
        TableView<TeachingAssistantPrototype> taTable = ohBuilder.buildTableView(OH_TAS_TABLE_VIEW, leftPane, CLASS_OH_TABLE_VIEW, ENABLED);
        taTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

        TableColumn nameColumn = ohBuilder.buildTableColumn(OH_NAME_TABLE_COLUMN, taTable, CLASS_OH_COLUMN);
        TableColumn emailColumn = ohBuilder.buildTableColumn(OH_EMAIL_TABLE_COLUMN, taTable, CLASS_OH_COLUMN);
        TableColumn slotsColumn = ohBuilder.buildTableColumn(OH_SLOTS_TABLE_COLUMN, taTable, CLASS_OH_CENTERED_COLUMN);
        TableColumn typeColumn = ohBuilder.buildTableColumn(OH_TYPE_TABLE_COLUMN, taTable, CLASS_OH_COLUMN);

        nameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("name"));
        emailColumn.setCellValueFactory(new PropertyValueFactory<String, String>("email"));
        slotsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("slots"));
        typeColumn.setCellValueFactory(new PropertyValueFactory<String, String>("type"));

        nameColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0 / 4.0));
        emailColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0 / 4.0));
        slotsColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0 / 4.0));
        typeColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0 / 4.0));

        // ADD BOX FOR ADDING A TA
        HBox taBox = ohBuilder.buildHBox(OH_ADD_TA_PANE, leftPane, CLASS_OH_PANE, ENABLED);
        ohBuilder.buildTextField(OH_NAME_TEXT_FIELD, taBox, CLASS_OH_TEXT_FIELD, ENABLED);
        ohBuilder.buildTextField(OH_EMAIL_TEXT_FIELD, taBox, CLASS_OH_TEXT_FIELD, ENABLED);
        ohBuilder.buildTextButton(OH_ADD_TA_BUTTON, taBox, CLASS_OH_BUTTON, !ENABLED);

        // MAKE SURE IT'S THE TABLE THAT ALWAYS GROWS IN THE LEFT PANE
        VBox.setVgrow(taTable, Priority.ALWAYS);

        // INIT THE HEADER ON THE RIGHT
        VBox rightPane = ohBuilder.buildVBox(OH_RIGHT_PANE, null, CLASS_OH_PANE, ENABLED);
        HBox officeHoursHeaderBox = ohBuilder.buildHBox(OH_OFFICE_HOURS_HEADER_PANE, rightPane, CLASS_OH_PANE, ENABLED);
        ohBuilder.buildLabel(OH_OFFICE_HOURS_HEADER_LABEL, officeHoursHeaderBox, CLASS_OH_HEADER_LABEL, ENABLED);

        // SETUP THE OFFICE HOURS TABLEc
        TableView<TimeSlot> officeHoursTable = ohBuilder.buildTableView(OH_OFFICE_HOURS_TABLE_VIEW, rightPane, CLASS_OH_OFFICE_HOURS_TABLE_VIEW, ENABLED);
        TableColumn startTimeColumn = ohBuilder.buildTableColumn(OH_START_TIME_TABLE_COLUMN, officeHoursTable, CLASS_OH_TIME_COLUMN);
        TableColumn endTimeColumn = ohBuilder.buildTableColumn(OH_END_TIME_TABLE_COLUMN, officeHoursTable, CLASS_OH_TIME_COLUMN);
        TableColumn mondayColumn = ohBuilder.buildTableColumn(OH_MONDAY_TABLE_COLUMN, officeHoursTable, CLASS_OH_DAY_OF_WEEK_COLUMN);
        TableColumn tuesdayColumn = ohBuilder.buildTableColumn(OH_TUESDAY_TABLE_COLUMN, officeHoursTable, CLASS_OH_DAY_OF_WEEK_COLUMN);
        TableColumn wednesdayColumn = ohBuilder.buildTableColumn(OH_WEDNESDAY_TABLE_COLUMN, officeHoursTable, CLASS_OH_DAY_OF_WEEK_COLUMN);
        TableColumn thursdayColumn = ohBuilder.buildTableColumn(OH_THURSDAY_TABLE_COLUMN, officeHoursTable, CLASS_OH_DAY_OF_WEEK_COLUMN);
        TableColumn fridayColumn = ohBuilder.buildTableColumn(OH_FRIDAY_TABLE_COLUMN, officeHoursTable, CLASS_OH_DAY_OF_WEEK_COLUMN);

        startTimeColumn.setCellValueFactory(new PropertyValueFactory<String, String>("startTime"));
        endTimeColumn.setCellValueFactory(new PropertyValueFactory<String, String>("endTime"));
        mondayColumn.setCellValueFactory(new PropertyValueFactory<String, String>("monday"));
        tuesdayColumn.setCellValueFactory(new PropertyValueFactory<String, String>("tuesday"));
        wednesdayColumn.setCellValueFactory(new PropertyValueFactory<String, String>("wednesday"));
        thursdayColumn.setCellValueFactory(new PropertyValueFactory<String, String>("thursday"));
        fridayColumn.setCellValueFactory(new PropertyValueFactory<String, String>("friday"));

        for (int i = 0; i < officeHoursTable.getColumns().size(); i++) {
            ((TableColumn) officeHoursTable.getColumns().get(i)).prefWidthProperty().bind(officeHoursTable.widthProperty().multiply(1.0 / 7.0));
        }

        // MAKE SURE IT'S THE TABLE THAT ALWAYS GROWS IN THE LEFT PANE
        VBox.setVgrow(officeHoursTable, Priority.ALWAYS);

        // BOTH PANES WILL NOW GO IN A SPLIT PANE
        SplitPane sPane = new SplitPane(leftPane, rightPane);
        sPane.setDividerPositions(.4);
        workspace = new BorderPane();

        // AND PUT EVERYTHING IN THE WORKSPACE
        ((BorderPane) workspace).setCenter(sPane);
    }

    public void initControllers() {
        OfficeHoursController controller = new OfficeHoursController((OfficeHoursApp) app);
        AppGUIModule gui = app.getGUIModule();

        // FOOLPROOF DESIGN STUFF
        TextField nameTextField = ((TextField) gui.getGUINode(OH_NAME_TEXT_FIELD));
        TextField emailTextField = ((TextField) gui.getGUINode(OH_EMAIL_TEXT_FIELD));

        nameTextField.textProperty().addListener(e -> {
            controller.processTypeTA();
        });
        emailTextField.textProperty().addListener(e -> {
            controller.processTypeTA();
        });

        // FIRE THE ADD EVENT ACTION
        nameTextField.setOnAction(e -> {
            controller.processAddTA();
        });
        emailTextField.setOnAction(e -> {
            controller.processAddTA();
        });

        ((Button) gui.getGUINode(OH_ADD_TA_BUTTON)).setOnAction(e -> {
            controller.processAddTA();
        });

        TableView officeHoursTableView = (TableView) gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);
        TableView tasTableView = (TableView) gui.getGUINode(OH_TAS_TABLE_VIEW);

        // Each radio button has their event
        ((RadioButton) gui.getGUINode(OH_RADIO_ALL)).setOnAction(e -> {
            if(!dataSet)
            {
                data = (OfficeHoursData) app.getDataComponent();
                dataSet = true;
            }
            // CLEAR THE TABLES
            addToHoldArray();
            tasTableView.getItems().clear();

            for (TeachingAssistantPrototype ta : holdTA) {
                tasTableView.getItems().add(ta);
            }
            for (int i = 0; i < officeHoursTableView.getItems().size(); i++) {
                TimeSlot timeslot = (TimeSlot) officeHoursTableView.getItems().get(i);
                timeslot.replaceNames();
            }
            refresh(tasTableView, officeHoursTableView);
        });

        ((RadioButton) gui.getGUINode(OH_RADIO_GRADUATE)).setOnAction(e -> {
            if(!dataSet)
            {
                data = (OfficeHoursData) app.getDataComponent();
                dataSet = true;
            }
            // CLEAR THE TABLES
            addToHoldArray();
            //officeHoursTableView.getItems().clear();
            tasTableView.getItems().clear();
//            ObservableList<TeachingAssistantPrototype> tass = data.getAssistants();
            for (TeachingAssistantPrototype ta : holdTA) {
                if (ta.getType().toLowerCase().equals("graduate")) {
                    tasTableView.getItems().add(ta);
                }
            }
            hideUnwantedTAs("graduate");
            refresh(tasTableView, officeHoursTableView);
        });

        ((RadioButton) gui.getGUINode(OH_RADIO_UNDERGRADUATE)).setOnAction(e -> {
            if(!dataSet)
            {
                data = (OfficeHoursData) app.getDataComponent();
                dataSet = true;

            }
            // CLEAR THE TABLES
            addToHoldArray();
            tasTableView.getItems().clear();

            for (TeachingAssistantPrototype ta : holdTA) {
                if (ta.getType().toLowerCase().equals("undergraduate")) {
                    tasTableView.getItems().add(ta);
                }
                
            }
            hideUnwantedTAs("undergraduate");
            refresh(tasTableView, officeHoursTableView);
        });

        officeHoursTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        officeHoursTableView.getSelectionModel().setCellSelectionEnabled(true);
        officeHoursTableView.setOnMouseClicked(e -> {
            controller.processToggleOfficeHours();
        });
        Button cut = (Button) gui.getGUINode(CUT_BUTTON);
        Button copy = (Button) gui.getGUINode(COPY_BUTTON);
//        cut.setOnAction((ActionEvent e) -> {
//            holdTA.remove((TeachingAssistantPrototype) tasTableView.getSelectionModel().getSelectedItem());
//        });

        tasTableView.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                TeachingAssistantPrototype ta = (TeachingAssistantPrototype) tasTableView.getSelectionModel().getSelectedItem();
                openDialog(ta);
                cut.setDisable(true);
                copy.setDisable(true);
            }
            if (e.getClickCount() == 1) {
                cut.setDisable(false);
                copy.setDisable(false);
                TeachingAssistantPrototype ta = (TeachingAssistantPrototype) tasTableView.getSelectionModel().getSelectedItem();
                selectAll(ta);
            }
        });

        // DON'T LET ANYONE SORT THE TABLES
        for (int i = 0; i < officeHoursTableView.getColumns().size(); i++) {
            ((TableColumn) officeHoursTableView.getColumns().get(i)).setSortable(false);
        }
        for (int i = 0; i < tasTableView.getColumns().size(); i++) {
            ((TableColumn) tasTableView.getColumns().get(i)).setSortable(false);
        }
    }

    // make appropriate cells .updateSelected(true);
    public void selectAll(TeachingAssistantPrototype ta) {
        AppGUIModule gui = app.getGUIModule();

        TableView officeHoursTableView = (TableView) gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);
        ObservableList<TableColumn> columns = officeHoursTableView.getColumns();
       
        TableViewSelectionModel selector = officeHoursTableView.getSelectionModel();
        selector.clearSelection();
        int rowNum = 0;
        for (Object row: officeHoursTableView.getItems()) {
            for (TableColumn column : columns) {
                try {
                    String cellVal = ((String) column.getCellData((TimeSlot) row));
                    if (!cellVal.contains(":")) {
                        if (new ArrayList<>(Arrays.asList(cellVal.split("\n"))).contains(ta.getName().split(" ")[0])) {
                            selector.select(rowNum,column);
                            selector.select(rowNum,column);
                        }
                    }
                } catch (NullPointerException e) {
                }
            }
            rowNum++;
        }

        for (int i = 0; i < officeHoursTableView.getItems().size(); i++) {
            officeHoursTableView.getSelectionModel();
        }
        

    }

    public void openDialog(TeachingAssistantPrototype ta) {
        // Get data from taTable before checking
        addToHoldArray();

        ArrayList<String> taNames = new ArrayList<>();
        ArrayList<String> taEmails = new ArrayList<>();
        ToggleGroup typeButton = new ToggleGroup();

        for (TeachingAssistantPrototype tas : holdTA) {
            taNames.add(tas.getName().toLowerCase());
            taEmails.add(tas.getEmail().toLowerCase());
        }

        Label editLabel = new Label("Edit Teaching Assistant");
        Label name = new Label("Name:  ");
        Label email = new Label("Email: ");
        Label type = new Label("Type:  ");

        RadioButton under = new RadioButton("Undergraduate");
        RadioButton gradu = new RadioButton("Graduate");

        under.setUserData("Undergraduate");
        gradu.setUserData("Graduate");

        under.setToggleGroup(typeButton);
        gradu.setToggleGroup(typeButton);

        TextField nameField = new TextField(ta.getName());
        TextField emailField = new TextField(ta.getEmail());

        Button ok = new Button("Ok");
        Button cancel = new Button("Cancel");

        VBox editBox = new VBox();

        HBox topLable = new HBox();
        topLable.getChildren().add(editLabel);

        HBox names = new HBox();
        names.getChildren().addAll(name, nameField);

        HBox emails = new HBox();
        emails.getChildren().addAll(email, emailField);

        HBox types = new HBox();
        types.getChildren().addAll(type, gradu, under);

        HBox okCancel = new HBox();
        okCancel.getChildren().addAll(ok, cancel);

        editBox.getChildren().addAll(topLable, names, emails, types, okCancel);

        editBox.setStyle("-fx-background-color: #DDE9E8");
        topLable.setStyle("-fx-padding: 8; -fx-font-size: 16pt; -fx-font-weight: bold; -fx-font-family: 'Monospaced';");
        names.setStyle("-fx-padding: 10; -fx-font-size: 12pt; -fx-font-weight: bold; -fx-font-family: 'Monospaced';-fx-alignment: CENTER_LEFT");
        emails.setStyle("-fx-padding: 10; -fx-font-size: 12pt; -fx-font-weight: bold; -fx-font-family: 'Monospaced';-fx-alignment: CENTER_LEFT");
        types.setStyle("-fx-padding: 10; -fx-font-size: 12pt; -fx-font-weight: bold; -fx-font-family: 'Monospaced';");
        okCancel.setStyle("-fx-padding: 10; -fx-font-size: 12pt; -fx-font-weight: bold; -fx-font-family: 'Monospaced'; -fx-alignment: CENTER;");
        gradu.setStyle("-fx-padding: 0 30 0 0;");
        nameField.setPrefWidth(400);
        emailField.setPrefWidth(400);
        ok.setPrefWidth(75);
        Scene editTA = new Scene(editBox, 500, 250);

        if (ta.getType().toLowerCase().equals("undergraduate")) {
            under.setSelected(true);
        } else {
            gradu.setSelected(true);
        }

        // Set the action on the textfield for foolproofing
        nameField.textProperty().addListener(e -> {
            String tasname = nameField.getText().toLowerCase();
            if (taNames.contains(tasname)
                    && !(ta.getName().toLowerCase().equals(tasname))) {
                nameField.setStyle("-fx-text-inner-color: red;");
                ok.setDisable(true);
            } else {
                nameField.setStyle("-fx-text-inner-color: black;");
                ok.setDisable(false);
            }
        });

        emailField.textProperty().addListener(e -> {
            String tasEmail = emailField.getText().toLowerCase();
            if ((taEmails.contains(tasEmail) && !(ta.getEmail().toLowerCase().equals(tasEmail))) || !(isLegalNewEmail(tasEmail))) {
                emailField.setStyle("-fx-text-inner-color: red;");
                ok.setDisable(true);
            } else {
                emailField.setStyle("-fx-text-inner-color: black;");
                ok.setDisable(false);
            }
        });

        // New window (Stage)
        Stage newWindow = new Stage();
        newWindow.setScene(editTA);

        ok.setOnAction((ActionEvent e) -> {
            newName = nameField.getText();
            newEmail = emailField.getText();
            newType = (String) typeButton.getSelectedToggle().getUserData().toString();

            TeachingAssistantPrototype newTA = new TeachingAssistantPrototype(newName, newEmail, newType);
            if (!newTA.equals(ta)) {
                AppGUIModule gui = app.getGUIModule();
                TableView tasTableView = (TableView) gui.getGUINode(OH_TAS_TABLE_VIEW);
                TableView officeHoursTableView = (TableView) gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);

                EditTA_Transaction taTransaction = new EditTA_Transaction(ta, newTA, gui);
                app.processTransaction(taTransaction);
                Collections.sort(tasTableView.getItems());
            }
            newWindow.close();
        });

        cancel.setOnAction(e -> {
            newWindow.close();
        });

        // Set position of second window, related to primary window.
        newWindow.show();

    }

    public boolean isLegalNewEmail(String email) {
        Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile(
                "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
        if (matcher.find()) {
            return true;
        } else {
            return false;
        }
    }

    public void addToHoldArray() {
        AppGUIModule gui = app.getGUIModule();

        TableView tasTableView = (TableView) gui.getGUINode(OH_TAS_TABLE_VIEW);
        if (tasTableView.getItems().size() > holdTA.size()) {
            holdTA = new ArrayList<>();
        }
        if (holdTA.isEmpty()) {
            holdOfficeHoursTable = (TableView) gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);
            for (int i = 0; i < tasTableView.getItems().size(); i++) {
                TeachingAssistantPrototype ta = (TeachingAssistantPrototype) tasTableView.getItems().get(i);
                holdTA.add(ta);
            }
        }
    }

    public void refresh(TableView a, TableView b) {
        a.refresh();

        for (int i = 0; i < b.getItems().size(); i++) {
            TimeSlot timeslot = (TimeSlot) b.getItems().get(i);
            timeslot.refresh();
        }
    }

    public void hideUnwantedTAs(String type) {
        AppGUIModule gui = app.getGUIModule();
        TableView officeHoursTableView = (TableView) gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);
        for (int i = 0; i < officeHoursTableView.getItems().size(); i++) {
            TimeSlot timeslot = (TimeSlot) officeHoursTableView.getItems().get(i);
            timeslot.replaceTAs(type);
        }
    }

    public void initFoolproofDesign() {
        AppGUIModule gui = app.getGUIModule();
        AppFoolproofModule foolproofSettings = app.getFoolproofModule();
        foolproofSettings.registerModeSettings(OH_FOOLPROOF_SETTINGS,
                new OfficeHoursFoolproofDesign((OfficeHoursApp) app));
    }

    @Override
    public void processWorkspaceKeyEvent(KeyEvent ke) {
        // WE AREN'T USING THIS FOR THIS APPLICATION
    }

    @Override
    public void showNewDialog() {
        // WE AREN'T USING THIS FOR THIS APPLICATION
    }
}
